import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedComponent } from '../components/feed/feed.component';
import { TokenService } from '../services/token.service';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { FeedSideComponent } from '../components/feed-side/feed-side.component';
import { PostFormComponent } from '../components/post-form/post-form.component';
import { PostsComponent } from '../components/posts/posts.component';
import { PostService } from '../services/post.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from '../components/users/users.component';
import { RouterModule } from '@angular/router';
import { UsersService } from '../services/users.service';
import { ChatComponent } from '../components/chat/chat.component';
import { MessageComponent } from '../components/message/message.component';
import { MessageService } from '../services/message.service';

@NgModule({
  declarations: [
    FeedComponent,
    NavbarComponent,
    FeedSideComponent,
    PostFormComponent,
    PostsComponent,
    UsersComponent,
    ChatComponent,
    MessageComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [FeedComponent, NavbarComponent],
  providers: [TokenService, PostService, UsersService, MessageService],
})
export class FeedModule {}
