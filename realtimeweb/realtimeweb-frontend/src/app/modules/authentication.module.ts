import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationTabsComponent } from '../components/authentication-tabs/authentication-tabs.component';
import { LoginComponent } from '../components/login/login.component';
import { SignupComponent } from '../components/signup/signup.component';
import { AuthenticationService } from '../services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AuthenticationTabsComponent, LoginComponent, SignupComponent],
  imports: [CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  exports: [AuthenticationTabsComponent, LoginComponent, SignupComponent],
  providers: [AuthenticationService],
})
export class AuthenticationModule {}
