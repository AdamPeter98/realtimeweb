import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from '../components/chat/chat.component';
import { FeedComponent } from '../components/feed/feed.component';
import { UsersComponent } from '../components/users/users.component';
import { AuthGuard } from '../services/auth.guard';

const routes: Routes = [
  {
    path: 'feed',
    component: FeedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'people',
    component: UsersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'chat/:name',
    component: ChatComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class FeedRoutingModule {}
