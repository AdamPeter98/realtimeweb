import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationTabsComponent } from '../components/authentication-tabs/authentication-tabs.component';

const routes: Routes = [
  {
    path: '',
    component: AuthenticationTabsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
