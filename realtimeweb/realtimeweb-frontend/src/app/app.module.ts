import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AuthenticationRoutingModule } from './modules/authentication-routing.module';
import { AuthenticationModule } from './modules/authentication.module';
import { FeedModule } from './modules/feed.module';
import { FeedRoutingModule } from './modules/feed-routing.module';
import { CookieModule, CookieService } from 'ngx-cookie';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenFilter } from './services/token-filter';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CookieModule.forRoot(),
    BrowserModule,
    AuthenticationModule,
    AuthenticationRoutingModule,
    FeedModule,
    FeedRoutingModule,
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenFilter,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
