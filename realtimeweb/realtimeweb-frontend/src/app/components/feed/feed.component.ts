import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
})
export class FeedComponent implements OnInit {
  token: any;

  constructor(private tokenService: TokenService) {}

  ngOnInit(): void {
    this.token = this.tokenService.GetPayload();
    console.log(this.token);
  }
}
