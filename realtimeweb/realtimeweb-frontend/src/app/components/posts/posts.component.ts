import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import io from 'socket.io-client';
import * as moment from 'moment';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  socket: any;
  posts: any[] = [];

  constructor(private postService: PostService) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit(): void {
    this.AllPosts();
    this.socket.on('refreshPage', () => {
      this.AllPosts();
    });
  }

  AllPosts() {
    this.postService.geAllPosts().subscribe((data) => {
      this.posts = data.posts;
      console.log(this.posts);
    });
  }

  TimeFromNow(time: moment.MomentInput) {
    return moment(time).fromNow();
  }
}
