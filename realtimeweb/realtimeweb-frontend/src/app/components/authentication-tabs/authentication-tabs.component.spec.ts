import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthenticationTabsComponent } from './authentication-tabs.component';

describe('AuthenticationTabsComponent', () => {
  let component: AuthenticationTabsComponent;
  let fixture: ComponentFixture<AuthenticationTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthenticationTabsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
