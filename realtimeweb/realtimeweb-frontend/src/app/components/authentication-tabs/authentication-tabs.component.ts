import { Component, OnInit } from '@angular/core';
import * as M from 'materialize-css';
@Component({
  selector: 'app-authentication-tabs',
  templateUrl: './authentication-tabs.component.html',
  styleUrls: ['./authentication-tabs.component.css'],
})
export class AuthenticationTabsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    M.AutoInit();
  }
}
