import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-feed-side',
  templateUrl: './feed-side.component.html',
  styleUrls: ['./feed-side.component.css'],
})
export class FeedSideComponent implements OnInit {
  user: any;

  constructor(private tokenService: TokenService) {}

  ngOnInit(): void {
    this.user = this.tokenService.GetPayload();
  }
}
