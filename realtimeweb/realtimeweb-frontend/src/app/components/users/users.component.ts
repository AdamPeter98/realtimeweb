import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { UsersService } from 'src/app/services/users.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: any[] = [];
  loggedInUser: any;

  constructor(private userService: UsersService, private tokenService: TokenService) {}

  ngOnInit(): void {
    this.loggedInUser = this.tokenService.GetPayload().data;
    this.GetUser();
  }

  GetUser() {
    this.userService.GetAllUsers().subscribe((data) => {
      _.remove(data.result, { username: this.loggedInUser.username });
      this.users = data.result;
    });
  }
}
