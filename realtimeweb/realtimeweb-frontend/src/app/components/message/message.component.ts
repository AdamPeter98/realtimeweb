import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';
import { TokenService } from 'src/app/services/token.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
})
export class MessageComponent implements OnInit {
  receivername: String | undefined;
  user: any;
  message: any;
  receiverData: any;

  constructor(
    private tokenService: TokenService,
    private msgService: MessageService,
    private route: ActivatedRoute,
    private userService: UsersService
  ) {}

  ngOnInit(): void {
    this.user = this.tokenService.GetPayload();
    this.route.params.subscribe((params) => {
      this.receivername = params.name;
      this.GetUserByUsername(this.receivername);
    });
  }

  GetUserByUsername(name: any) {
    this.userService.GetUserByName(name).subscribe((data) => {
      this.receiverData = data.result;
    });
  }

  SendMessage() {
    console.log(this.user.data._id);
    this.msgService
      .SendMessage(
        this.user.data._id,
        this.receiverData._id,
        this.receiverData.username,
        this.message
      )
      .subscribe((data) => {
        console.log('hello', data);
        this.message = '';
      });
  }
}
