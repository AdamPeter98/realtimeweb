import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const BASEURL = 'http://localhost:3000/api/realtimeweb';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  registerUser(body: any): Observable<any> {
    return this.http.post(`${BASEURL}/register`, body);
  }

  loginUser(body: any): Observable<any> {
    return this.http.post(`${BASEURL}/login`, body);
  }
}
