import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor(private cookieService: CookieService) {}

  SetToken(token: string | undefined) {
    this.cookieService.put('auth_token', token);
  }

  GetToken() {
    return this.cookieService.get('auth_token');
  }

  DeleteToken() {
    this.cookieService.remove('auth_token');
  }

  GetPayload() {
    const token = this.GetToken();
    let payload;

    if (token) {
      payload = token.split('.')[1];
      payload = JSON.parse(window.atob(payload));
    }

    return payload;
  }
}
