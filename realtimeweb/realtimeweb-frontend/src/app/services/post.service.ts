import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const BASEURL = 'http://localhost:3000/api/realtimeweb';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private http: HttpClient) {}

  addPost(body: any): Observable<any> {
    return this.http.post(`${BASEURL}/post/add-post`, body);
  }

  geAllPosts(): Observable<any> {
    return this.http.get(`${BASEURL}/posts`);
  }
}
