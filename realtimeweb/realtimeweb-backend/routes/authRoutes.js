const router = require("express").Router();

const AuthController = require("../controllers/authController");

router.post("/register", AuthController.CreateUser);

router.post("/login", AuthController.LoginUser);

module.exports = router;
