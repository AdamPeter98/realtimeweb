const router = require("express").Router();

const PostsController = require("../controllers/postsController");
const AuthFilter = require("../utils/authenticationFilter");

router.get("/posts", AuthFilter.checkToken, PostsController.GetAllPosts);

router.post("/post/add-post", AuthFilter.checkToken, PostsController.AddPost);

module.exports = router;
