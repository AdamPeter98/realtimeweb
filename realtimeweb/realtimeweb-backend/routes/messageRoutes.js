const router = require("express").Router();

const MessageController = require("../controllers/messagesController");
const AuthFilter = require("../utils/authenticationFilter");

router.post(
  "/chat-messages/:sender_Id/:receiver_Id",
  AuthFilter.checkToken,
  MessageController.SendMessage
);

module.exports = router;
