const express = require("express");

const router = express.Router();

const UserController = require("../controllers/usersController");
const AuthFilter = require("../utils/authenticationFilter");

router.get("/users", AuthFilter.checkToken, UserController.GetAllUsers);

router.get("/user/:id", AuthFilter.checkToken, UserController.GetUser);

router.get(
  "/username/:name",
  AuthFilter.checkToken,
  UserController.GetUserByName
);

module.exports = router;
