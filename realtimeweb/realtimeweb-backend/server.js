const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const { Server } = require("socket.io");

const app = express();
const http = require("http");
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: "http://localhost:4200",
  },
});

const config = require("./config/applicationConfiguration");
const utils = require("./utils/applicationUtils");
const auth = require("./routes/authRoutes");
const posts = require("./routes/postRoutes");
const users = require("./routes/userRoutes");
const message = require("./routes/messageRoutes");

app.use(cors());

app.use((req, res, next) => {
  utils.addHttpHeaders(res, config);
  next();
});

app.use(express.json({ limit: config.jsonLimit }));
app.use(express.urlencoded({ extended: true, limit: config.jsonLimit }));
app.use(cookieParser());

mongoose.Promise = global.Promise;
mongoose.connect(config.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

require("./socket/streams")(io);

app.use(config.apiBaseUrl, auth);
app.use(config.apiBaseUrl, posts);
app.use(config.apiBaseUrl, users);
app.use(config.apiBaseUrl, message);

server.listen(config.testEnvPort, () => {
  console.log(`Running on port ${config.testEnvPort}`);
});
