module.exports = {
  apiBaseUrl: "/api/realtimeweb",
  tokenExpirationTime: 10000,
  jwtTokenKey: "auth_token",
  jwtSalt: 10,
  testEnvPort: 3000,
  url: "mongodb://localhost:27017/realtimeweb",
  secret: "realtimewebsecret",
  jsonLimit: "50mb",
  httpRequestHeaders: {
    "Access-Control-Allow-Origin": ["*"],
    "Access-Control-Allow-Credentials": ["true"],
    "Access-Control-Allow-Methods": ["*"],
    "Access-Control-Allow-Headers": [
      "Origin, X-Requested-With, Content-Type, Accept",
    ],
  },
};
