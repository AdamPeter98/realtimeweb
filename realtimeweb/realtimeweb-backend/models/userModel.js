const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  username: { type: String },
  email: { type: String },
  password: { type: String },
  posts: [
    {
      postId: { type: mongoose.Schema.Types.ObjectId, ref: "Post" },
      post: { type: String },
      create: { type: Date, deafult: Date.now() },
    },
  ],
  chatList: [
    {
      receiverId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      msgId: { type: mongoose.Schema.Types.ObjectId, ref: "Message" },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
