module.exports = {
  firstUpper: (username) => {
    const name = username.toLowerCase();
    return name.charAt(0).toUpperCase() + name.slice(1);
  },

  lowerCase: (str) => {
    return str.toLowerCase();
  },

  addHttpHeaders: (res, appProperties) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true"),
      res.header(
        "Access-Control-Allow-Methods",
        "GET",
        "POST",
        "DELETE",
        "PUT",
        "OPTIONS"
      ),
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Accept, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers"
      );
    return res;
  },
};
