const jwt = require("jsonwebtoken");
const config = require("../config/applicationConfiguration");
const HttpStatus = require("http-status-codes");

module.exports = {
  checkToken: (req, res, next) => {
    if (!req.headers.authorization) {
      return res
        .status(HttpStatus.StatusCodes.UNAUTHORIZED)
        .json({ mesasge: "No authorization" });
    }

    const token = req.headers.authorization.split(" ")[1];
    if (!token) {
      return res
        .status(HttpStatus.StatusCodes.FORBIDDEN)
        .json({ mesasge: "No token provided" });
    }

    return jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        if (err.expiredAt < new Date()) {
          return res.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: "token expired, please log in again",
            token: null,
          });
        }
        next();
      }
      req.user = decoded.data;
      next();
    });
  },
};
