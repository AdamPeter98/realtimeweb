const User = require("../models/userModel");
const utils = require("../utils/applicationUtils");
const bcrypt = require("bcryptjs");

module.exports = {
  validateUser: async (req) => {
    return await User.findOne({
      email: utils.lowerCase(req.body.email),
    });
  },

  validateUserName: async (req) => {
    return await User.findOne({
      username: utils.firstUpper(req.body.username),
    });
  },

  validateUserByToken: (req, user) => {
    bcrypt.compare(req.body.password, user.password).then((result) => {
      return result;
    });
  },
};
