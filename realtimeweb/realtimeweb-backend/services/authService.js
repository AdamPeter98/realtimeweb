const Joi = require("joi");
const config = require("../config/applicationConfiguration");
const utils = require("../utils/applicationUtils");
const User = require("../models/userModel");
const HttpStatus = require("http-status-codes");
const JWT = require("jsonwebtoken");
const { jwtTokenKey } = require("../config/applicationConfiguration");

module.exports = {
  getSchema: () => {
    return Joi.object({
      username: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string().email().required(),
    });
  },

  getUserSchemaPost: () => {
    return Joi.object({
      post: Joi.string().required(),
    });
  },

  insertUser: (res, value, hash) => {
    const body = {
      username: utils.firstUpper(value.username),
      email: utils.lowerCase(value.email),
      password: hash,
    };

    User.create(body)
      .then((user) => {
        const token = JWT.sign({ data: user }, config.secret, {
          expiresIn: config.tokenExpirationTime,
        });

        res.cookie(config.jwtTokenKey, token);
        res
          .status(HttpStatus.StatusCodes.CREATED)
          .json({ message: "User created successfully", user, token });
      })
      .catch((err) => {
        res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Token generation error " + err });
      });
  },

  createToken: (res, user) => {
    const token = JWT.sign({ data: user }, config.secret, {
      expiresIn: config.tokenExpirationTime,
    });
    res.cookie(jwtTokenKey, token);
    return res
      .status(HttpStatus.StatusCodes.OK)
      .json({ message: "Login successful", user, token });
  },
};
