const User = require("../models/userModel");
const HttpStatus = require("http-status-codes");

module.exports = {
  async GetAllUsers(req, res) {
    await User.find({})
      .populate("posts.postsId")
      .then((result) => {
        res.status(HttpStatus.StatusCodes.OK).json({ result });
      })
      .catch((err) => {
        res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Error ocured", error: err });
      });
  },

  async GetUser(req, res) {
    await User.findOne({ _id: req.params.id })
      .populate("posts.postsId")
      .then((result) => {
        res.status(HttpStatus.StatusCodes.OK).json({ result });
      })
      .catch((err) => {
        res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Error ocured", error: err });
      });
  },

  async GetUserByName(req, res) {
    await User.findOne({ username: req.params.name })
      .populate("posts.postsId")
      .then((result) => {
        res.status(HttpStatus.StatusCodes.OK).json({ result });
      })
      .catch((err) => {
        res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Error ocured", error: err });
      });
  },
};
