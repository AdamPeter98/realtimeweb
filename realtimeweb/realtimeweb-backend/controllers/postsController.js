const authService = require("../services/authService");
const Post = require("../models/postModel");
const HttpStatus = require("http-status-codes");
const User = require("../models/userModel");

module.exports = {
  AddPost(req, res) {
    const schema = authService.getUserSchemaPost();
    const { error } = schema.validate(req.body);

    if (error && error.details) {
      return res
        .status(HttpStatus.StatusCodes.BAD_REQUEST)
        .json({ msg: error.details });
    }

    const body = {
      user: req.user._id,
      username: req.user.username,
      post: req.body.post,
      created: new Date(),
    };

    Post.create(body)
      .then(async (post) => {
        await User.updateOne(
          {
            _id: req.user._id,
          },
          {
            $push: {
              posts: {
                postId: post._id,
                post: req.body.post,
                created: new Date(),
              },
            },
          }
        );
        res
          .status(HttpStatus.StatusCodes.OK)
          .json({ message: "Post created", post });
      })
      .catch((err) => {
        res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Error occured" });
      });
  },

  async GetAllPosts(req, res) {
    try {
      const posts = await (await Post.find({}).populate("user")).reverse();

      return res
        .status(HttpStatus.StatusCodes.OK)
        .json({ message: "All posts", posts });
    } catch (err) {
      return res
        .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
        .json({ message: "Error occured during displaying posts", posts });
    }
  },
};
