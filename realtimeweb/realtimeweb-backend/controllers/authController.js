const HttpStatus = require("http-status-codes");
const bcrypt = require("bcryptjs");

const User = require("../models/userModel");
const utils = require("../utils/applicationUtils");
const authService = require("../services/authService");
const applicationConfiguration = require("../config/applicationConfiguration");

module.exports = {
  async CreateUser(req, res) {
    const schema = authService.getSchema();

    const { error, value } = schema.validate(req.body);

    if (error && error.details) {
      return res
        .status(HttpStatus.StatusCodes.BAD_REQUEST)
        .json({ msg: error.details });
    }

    const userEmail = await User.findOne({
      email: utils.lowerCase(req.body.email),
    });

    if (userEmail) {
      return res
        .status(HttpStatus.StatusCodes.CONFLICT)
        .json({ message: "Email already exist" });
    }

    const userName = await User.findOne({
      username: utils.firstUpper(req.body.username),
    });

    if (userName) {
      return res
        .status(HttpStatus.StatusCodes.CONFLICT)
        .json({ message: "Username already exists" });
    }

    return bcrypt.hash(
      value.password,
      applicationConfiguration.jwtSalt,
      (err, hash) => {
        if (err) {
          return res
            .status(HttpStatus.StatusCodes.BAD_REQUEST)
            .json({ message: "Error hashing password" });
        }
        authService.insertUser(res, value, hash);
      }
    );
  },

  async LoginUser(req, res) {
    if (!req.body.username || !req.body.password) {
      return res
        .status(HttpStatus.StatusCodes.NOT_FOUND)
        .json({ message: "No empty fields allowd" });
    }

    await User.findOne({ username: utils.firstUpper(req.body.username) })
      .then((user) => {
        if (!user) {
          return res
            .status(HttpStatus.StatusCodes.NOT_FOUND)
            .json({ message: "User name not found" });
        }
        return bcrypt
          .compare(req.body.password, user.password)
          .then((result) => {
            if (!result) {
              return res
                .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
                .json({ message: "password is incorrect" });
            }

            return authService.createToken(res, user);
          });
      })
      .catch((error) => {
        return res
          .status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
          .json({ message: "Error occured " + error });
      });
  },
};
